//https://www.youtube.com/watch?v=Rb93uLXiTwA  donload tesseract

const express = require("express");
const tesseract = require("node-tesseract-ocr");
const path = require("path");

const app = express();
app.use(express.static(path.join(__dirname + "/images")));

app.get("/", (req, res) => {
  const config = {
    lang: "eng", // default
    oem: 3,
    psm: 3,
    // load_system_dawg: 0,
    // tessedit_char_whitelist: "0123456789",
    // presets: ["tsv"],
  };

  tesseract
    .recognize(__dirname + "/images/one.png", config)
    .then((text) => {
      console.log("Result:", text);
      //   let d = parseInt(text)
      return res.send(text);
      return res.json({
        text,
      });
    })
    .catch((error) => {
      console.log(error.message);
    });
  //   res.json({ message: "Hello OCR" });
});

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
